package com.abdulbasitmehtab.flagsoftheworld;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.support.v7.widget.Toolbar;

public class SecondActivity extends AppCompatActivity {

    Toolbar mToolbar;
    ImageView flag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        flag = (ImageView) findViewById(R.id.flag);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mToolbar.setTitle(bundle.getString("CountryName"));
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Australia")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.australia));
            }
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Brazil")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.brazil));
            }
            if (mToolbar.getTitle().toString().equalsIgnoreCase("China")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.china));
            }
            if (mToolbar.getTitle().toString().equalsIgnoreCase("France")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.france));
            }
            if (mToolbar.getTitle().toString().equalsIgnoreCase("Germany")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.germany));
            }
            if (mToolbar.getTitle().toString().equalsIgnoreCase("India")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.india));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Ireland")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.ireland));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Italy")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.italy));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Mexico")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.mexico));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Pakistan")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.pakistan));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Poland")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.poland));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Russia")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.russia));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("Spain")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.spain));
            }
            if(mToolbar.getTitle().toString().equalsIgnoreCase("United States")) {
                flag.setImageDrawable(ContextCompat.getDrawable(SecondActivity.this,
                        R.drawable.us));
            }
        }
    }
}